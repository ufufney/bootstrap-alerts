/**
 * Created by ufufney on 22.12.14.
 *
 * Using:
 *
 * $.alert({
 *  title: 'Hello, world',
 *  text: 'You're using my alerts right now!',
 *  buttons: {
 *   success: 'Success button',
 *   cancel:  'Cancel button',
 *  },
 *  success: function() {
 *   console.log('Success button callback');
 *  },
 *  cancel: function() {
 *   console.log('Cancel button callback');
 *  },
 *  close: function() {
 *   console.log('Close button callback');
 *  }
 * });
 */

jQuery.alert = function(params) {
    if (!$('#modal-system-alert').length) {
        var $modalContent = $('<div>', {
                'class': 'modal-content'
            }),
            $closeButton = $('<button>', {
                'class': 'close',
                'data-dismiss': 'modal',
                'aria-hidden': 'true',
                'html': '&times;'
            }).on('click', params.buttons.close ? params.close : function(){}),
            $okButton = $('<button>', {
                'type': 'button',
                'class': 'btn btn-primary',
                'data-dismiss': 'modal',
                'html': (params.buttons.success) ? params.buttons.success : 'Ok'
            }).on('click', params.success ? params.success() : function(){}),
            $cancelButton = params.buttons.cancel ?
                $('<button>', {
                    'type': 'button',
                    'class': 'btn btn-danger',
                    'data-dismiss': 'modal',
                    'html': params.buttons.cancel ? params.buttons.cancel : 'Cancel'
                }).on('click', params.cancel ? params.cancel : function(){}) : null;

        $('<div>', {
            'class': 'modal-header',
            html: $closeButton
        })
            .append('<h4 class="modal-title" id="myModalLabel">' + params.title + '</h4>')
            .appendTo($modalContent);

        $('<div>', {
            'class': 'modal-body',
            text: params.text
        }).appendTo($modalContent);

        $('<div>', {
            'class': 'modal-footer'
        })
            .append($cancelButton || '')
            .append($okButton)
            .appendTo($modalContent);

        $('<div>', {
            'class': 'modal fade',
            'id': 'modal-system-alert',
            'tabindex': '-1',
            'role': 'dialog',
            html: $('<div>', {
                'class': 'modal-dialog',
                html: $modalContent
            })
        }).appendTo('body');
    }

    $('#modal-system-alert').modal('show');
};
