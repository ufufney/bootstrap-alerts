Using:


```
#!jquery

$.alert({
 title: 'Hello, world',
 text: 'You're using my alerts right now!',
 buttons: {
  success: 'Success button',
  cancel:  'Cancel button',
 },
 success: function() {
  console.log('Success button callback');
 },
 cancel: function() {
  console.log('Cancel button callback');
 },
 close: function() {
  console.log('Close button callback');
 }
});
```
